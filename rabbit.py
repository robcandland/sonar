#!/usr/bin/env python3

import requests
import argparse
import json
import re
import logging as log
import sys


def percent(part, whole):
    percent = float(100.0 * float(part)/float(whole))
    log.debug("Percent used: 100.0 * {0}/{1} is {2}".format(part, whole, percent))
    return float("{:.2f}".format(percent))


def getjson(url):
    try:
        log.info("About to try {0}".format(url))
        # Adding verify=False to try and get Sonar to flag
        r = requests.get(url, auth=up, timeout=20)
        j = r.json()
        r.raise_for_status()
    except Exception as e:
        print("CRITICAL - RabbitMQ not responding - {0}|".format(e))
        sys.exit(2)
    d = json.dumps(j, indent=4)
    log.info("Got JSON:")
    log.debug(d)
    return j


parser = argparse.ArgumentParser(description="RabbitMQ Health Checks")
parser.add_argument("-v", "--verbose", action="count", help="Output verbose messages", default=0)
parser.add_argument("-u", "--user", dest="user", help="User")
parser.add_argument("-p", "--password", dest="password", help="Password",
                    default="notrealpasswor*!J(8P5fJ[^Wf554550fb33fnotrealpassworddiescrapers")
parser.add_argument("-P", "--port", dest="port", help="Port, default is 15672", default=15672)
parser.add_argument("--fd_percent", dest="fd_percent", default=70, type=int,
                    help="File Descriptors percent threshold, default 70%")
parser.add_argument("--proc_percent", dest="proc_percent", default=60, type=int,
                    help="Erlang Processes percent threshold, default 60%")
parser.add_argument("--sockets_percent", dest="sockets_percent", default=60, type=int,
                    help="Socket Descriptors percent threshold, default 80%")
parser.add_argument("--mem_percent", dest="mem_percent", default=70, type=int,
                    help="Memory usage percent threshold, default 70%")
parser.add_argument("--messages_threshold", dest="messages_threshold", default=20000, type=int,
                    help="")
parser.add_argument("hostname", help="Rabbit MQ Node hostname")
options = parser.parse_args()

baseurl = "https://" + options.hostname + ":" + str(options.port)
up = (options.user, options.password)

levels = [log.WARNING, log.INFO, log.DEBUG]
level = levels[min(len(levels)-1, options.verbose)]
log.basicConfig(level=level, format="%(asctime)s %(levelname)s %(message)s")

log.info("")
log.info("Starting RabbitMQ Health Check")

# Create/consume messages
log.info("")
log.info("Aliveness test:")
aliveurl = baseurl + "/api/aliveness-test/" + "%2F"
alive = getjson(aliveurl)
try:
    a = alive['status']
    log.info("Got aliveness-test result, status is {0}".format(alive['status']))
except Exception as e:
    print("CRITICAL - RabbitMQ status is not OK - {0}|".format(e))
    sys.exit(2)
if a != "ok":
    print("CRITICAL - RabbitMQ status is not OK|")
    sys.exit(2)

# Check to see if we have Network Paritions and check descriptors and erlang procs
# Checking to see if sonar flags this:
# log.info("Network Partition and file/socket descriptors and Erlang procs check:")
log.info("")
log.info("Network Partition and file/socket descriptors and Erlang procs check:")
netparurl = baseurl + "/api/nodes"
nodejson = getjson(netparurl)
log.debug("len of nodejson JSON is {0}, range is {1}".format(len(nodejson), range(len(nodejson))))
for index in range(len(nodejson)):
    nodename = re.sub('rabbit@', '', nodejson[index]['name'])
    if re.search(nodename, options.hostname):
        log.debug("Partitions is {0}".format(nodejson[index]['partitions']))
        log.debug("len of Partitions is {0}".format(len(nodejson[index]['partitions'])))
        if len(nodejson[index]['partitions']) != 0:
            print("CRITICAL - RabbitMQ split-brain detected, network partitions present|")
            sys.exit(2)
        else:
            log.info("No network partitions present for {0}, moving on".format(nodename))
        # Now check file/socket descriptors or erlang procs, but by host
        log.debug("nodename is {0}".format(nodename))
        log.debug("Got our truncated hostname {0} in JSON".format(options.hostname))
        fd_used = nodejson[index]['fd_used']
        fd_total = nodejson[index]['fd_total']
        proc_used = nodejson[index]['proc_used']
        proc_total = nodejson[index]['proc_total']
        sockets_used = nodejson[index]['sockets_used']
        sockets_total = nodejson[index]['sockets_total']
        mem_used = nodejson[index]['mem_used']
        mem_limit = nodejson[index]['mem_limit']
        ournodename = nodename

fd_percent = percent(fd_used, fd_total)
proc_percent = percent(proc_used, proc_total)
sockets_percent = percent(sockets_used, sockets_total)
mem_percent = percent(mem_used, mem_limit)
metrics = ["fd_percent", "proc_percent", "sockets_percent", "mem_percent"]
for imetric in metrics:
    log.info("nodename {0} - {1} is {2}% used".format(ournodename, imetric, eval(imetric)))

if sockets_percent > options.sockets_percent:
    print("CRITICAL - Socket Descriptor usage exceeds {0}%|".format(options.sockets_percent))
    sys.exit(2)
if fd_percent > options.fd_percent:
    print("CRITICAL - File Descriptor usage exceeds {0}%|".format(options.fd_percent))
    sys.exit(2)
if proc_percent > options.proc_percent:
    print("CRITICAL - Erlang Process usage exceeds {0}%|".format(options.proc_percent))
    sys.exit(2)
if mem_percent > options.mem_percent:
    print("CRITICAL - Erlang Memory usage exceeds {0}%|".format(options.mem_percent))
    sys.exit(2)

# Get stats
log.info("")
log.info("Getting statistics via /api/overview")
overviewurl = baseurl + "/api/overview"
overview = getjson(overviewurl)

try:
    messages = overview['queue_totals']['messages']
except KeyError:
    messages = 0
try:
    mespersec = overview['queue_totals']['messages_details']['rate']
except KeyError:
    mespersec = 0
try:
    pubpersec = overview['message_stats']['publish_details']['rate']
except KeyError:
    pubpersec = 0
try:
    ackpersec = overview['message_stats']['ack_details']['rate']
except KeyError:
    ackpersec = 0
try:
    getackpersec = overview['message_stats']['deliver_get_details']['rate']
except KeyError:
    getackpersec = 0
try:
    getnoackpersec = overview['message_stats']['get_no_ack_details']['rate']
except KeyError:
    getnoackpersec = 0
try:
    delpersec = overview['message_stats']['deliver_details']['rate']
except KeyError:
    delpersec = 0
try:
    delnoackpersec = overview['message_stats']['deliver_no_ack_details']['rate']
except KeyError:
    delnoackpersec = 0

log.info("Got messages -> {0}".format(messages))
log.info("Got mpersec -> {0}".format(mespersec))
log.info("Got pubpersec -> {0}".format(pubpersec))
log.info("Got ackpersec -> {0}".format(ackpersec))
log.info("Got getackpersec -> {0}".format(getackpersec))
log.info("Got getnoackpersec -> {0}".format(getnoackpersec))
log.info("Got delpersec -> {0}".format(delpersec))
log.info("Got delnoackpersec - {0}".format(getnoackpersec))

if int(messages) > options.messages_threshold:
    print("Queue messages of {0} exceeds threshold of {1}|"
          .format(messages, options.messages_threshold))

print("OK - Checks completed|")
sys.exit(0)
# EOF
